// a simple float colour shader
#version 410

in vec2 vTexCoord;

uniform sampler2D diff;

out vec4 FragColour;

uniform mat4 ProjectionViewModel;

void main() {
	FragColour = texture(diff, vTexCoord);
}