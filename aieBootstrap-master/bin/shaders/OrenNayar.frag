// classic Phong fragment shader
#version 410

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;
/////////////////////////
in vec3 vTangent;															
in vec3 vBiTangent;	
////////////////////////													

// Compiler would strip these out if not used
uniform vec3 Ia; // ambient light colour
uniform vec3 Id; // diffuse light colour
uniform vec3 Is; // specular light colour
uniform vec3 lightDirection;

uniform vec3 Ka; // ambient material colour
uniform vec3 Kd; // diffuse material colour
uniform vec3 Ks; // specular material colour
uniform float specularPower; // material specular power

uniform sampler2D diffuseTexture;
//////////////////////////////////////
uniform sampler2D specularTexture;										
uniform sampler2D normalTexture;
//////////////////////////////////////
uniform float roughness;
//////////////////////////////////////	

uniform vec3 cameraPosition;

out vec4 FragColour;

void main() {
	// ensure normal and light direction are normalised
	vec3 N = normalize(vNormal);
	/////////////////////////////////
	vec3 T = normalize(vTangent);
	vec3 BT = normalize(vBiTangent);
	/////////////////////////////////
	vec3 L = normalize(lightDirection);
	/////////////////////////////////////////////////////////////////
 	mat3 TBN = mat3(T,BT,N);
 	
 	vec3 texDiffuse = texture(diffuseTexture, vTexCoord).rgb;
 	vec3 texSpecular = texture(specularTexture, vTexCoord).rgb;
 	vec3 texNormal = texture(normalTexture, vTexCoord).rgb;
	
	N = TBN * (texNormal * 2 - 1);
	
	/////////////////////////////////////////////////////////////////
	// calculate lambert term (negate light direction)
	float lambertTerm = max (0, min(1, dot(N, -L) ));
	
	// calculate view vector and reflection vector
	vec3 V = normalize(cameraPosition - vPosition.xyz);
	vec3 R = reflect(L, N);
	
	/////////////////////////////////////////////////////////////////
	
	float NdL = max( 0.0f, dot (N, -L));
	float NdE = max( 0.0f, dot (N, V));
	
	float R2 = roughness * roughness;
	
	// Oren-Nayar Diffuse Term
	float A = 1.0f -0.5f * R2 / (R2 + 0.33f);
	float B = 0.45f * R2 / (R2 + 0.09f);
	
	// CX = Max (0, cos(1, e))
	vec3 lightProjected = normalize(L - N * NdL);
	vec3 viewProjected = normalize(V - N * NdE);
	float CX = max (0.0f, dot ( lightProjected, viewProjected));
	
	// DX = sin (alpha) * tam (beta)
	float alpha = sin ( max (acos (NdE), acos (NdL)));
	float beta = tan (min (acos (NdE), acos (NdL)));
	float DX = alpha * beta;
	
	// Calculate Oren-Nayar, replaces the Phong Lambertian Term
	float orenNayar = NdL * (A + B * CX * DX);
	////////////////////////////////////////////////////////////////
	
	// calculate specular term
	float specularTerm = pow(max (0, dot (R, V)), specularPower);
	
	// calculate diffuse
	vec3 ambient = Ia * Ka;
	vec3 diffuse = Id * Kd * texDiffuse * orenNayar;
	vec3 specular = Is * Ks /* * texSpecular*/ * specularTerm;
	
	// output final colour
	FragColour = vec4 (ambient + diffuse + specular, 1)  /* * texture(diffuseTexture, vTexCoord)*/;
	//* texture (diff, vTexCoord)
	//FragColour = vec4 (ambient + diffuse , 0) * texture (diff, vTexCoord) + vec4(specular, 1);
}