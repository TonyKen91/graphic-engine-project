#pragma once

#include <glm\vec2.hpp>
#include <glm\vec4.hpp>
#include <glm\ext.hpp>
#include <Gizmos.h>

class Scene;
class Sphere;
class Plane;
class Box;

enum ShapeType {
	JOINT = -1,
	PLANE = 0,
	SPHERE,
	BOX,
};

const int 	SHAPE_COUNT = 3;

class GameObject
{
protected:
	GameObject(ShapeType a_shapeID) : m_shapeID(a_shapeID) {}

public:
	virtual void fixedUpdate(glm::vec2 gravity, float timeStep) = 0;
	virtual void draw() = 0;
	virtual float getTotalEnergy() = 0;
	virtual float getLinearKineticEnergy() = 0;
	virtual float getRotationalKineticEnergy() = 0;
	virtual void setGameScene(Scene* scene);

	virtual void collideWithSphere(Sphere* pOther) = 0;
	virtual void collideWithPlane(Plane* pOther) = 0;
	virtual void collideWithBox(Box* pOther) = 0;
	virtual void collide(GameObject* obj) = 0;

	~GameObject();
	ShapeType getShapeID() { return m_shapeID; }
	bool inGrid = false;


protected:
	ShapeType m_shapeID;
	Scene* m_scene = nullptr;
};

