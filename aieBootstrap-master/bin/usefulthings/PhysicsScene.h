#pragma once

#include <glm\vec2.hpp>
#include <iostream>
#include <list>

#include "Sphere.h"
#include "Rigidbody.h"

using std::cout;

class Scene
{
public:
	Scene();
	~Scene();

	void addActor(GameObject* actor);
	void removeActor(GameObject* actor);
	void update(float dt);
	void draw();

	void setGravity(const glm::vec2 gravity) { m_gravity = gravity; }
	glm::vec2 getGravity() const { return m_gravity; }

	void setTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float getTimeStep() const { return m_timeStep; }

	void checkForCollision();

	void addToRemoveList(GameObject* actor);

	bool isGameOver = false;
	Sphere* centreSphere = nullptr;
	float	restrictedRadius = 50;
	bool	victoryCondition = false;

	float totalEnergy;
	float linearKinetic;
	float rotationalKinetic;

protected:

	glm::vec2 m_gravity;
	float m_timeStep;
	int m_ballsInGrid;
	std::list<GameObject*> m_actors;
	std::list<GameObject*> m_collidable;
	std::list<GameObject*> m_deletedObjects;
};

