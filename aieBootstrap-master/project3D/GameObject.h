#pragma once
#include <glm/mat4x4.hpp>
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "tiny_obj_loader.h"
#include "RenderTarget.h"
#include "Light.h"

class GameObject
{
public:
	GameObject();
	GameObject(const char * vertexFilename, const char * fragmentFilename, aie::OBJMesh mesh);
	~GameObject();


	void update(float deltaTime);
	void draw();



	// Getter functions
	glm::vec3 getPosition();


	// Setter functions
	void setVert(const char *filenae);
	void setFrag(const char *filenae);
	void setMesh();


	void setPosition(glm::vec3 position);
	void setTransform(glm::mat4 transform);


private:
	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;


	bool					m_vertexChanged;
	bool					m_fragmentChanged;

	const char*				m_vertFilename;
	const char*				m_fragFilename;

	aie::ShaderProgram		m_shader;
	aie::OBJMesh			m_mesh;

	glm::mat4				m_transform;

	glm::vec3				m_position;


	Camera				m_camera;
	Light				m_light;
	glm::vec3			m_ambientLight;

	float				m_roughness = 0.0f;
	float				m_reflectionCoefficient = 0.0f;
};

