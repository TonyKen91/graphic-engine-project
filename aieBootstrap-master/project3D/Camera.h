#pragma once

#include <glm\ext.hpp>
#include <Input.h>


class Camera
{
public:
	Camera() : m_theta(0), m_phi(-20), m_position(-10, 4, 0) {}

	glm::mat4 GetProjectMatrix(float w, float h);
	glm::mat4 GetViewMatrix();

	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);

	void Update();



private:
	float m_theta;
	float m_phi;
	glm::vec3 m_position;

	int m_lastMouseX;
	int m_lastMouseY;

	int m_lastScroll;
};