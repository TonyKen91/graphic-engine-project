#include "GameObject.h"



GameObject::GameObject()
{
	// load vertex shader from file
	m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");

	// load fragment shader from file
	m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	if (m_shader.link() == false)
	{
		printf("Shader Error: %s\n", m_shader.getLastError());
	}

	// set default transform
	m_transform = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };

	m_light.direction = { 0.0f, 0.0f, 0.0f };
	m_light.diffuse = { 1, 1, 1 };
	m_light.specular = { 1.0f, 1.0f, 1.0f };
	m_ambientLight = { 0.25f, 0.25f, 0.25f };

	m_vertexChanged = false;
	m_fragmentChanged = false;
}


GameObject::GameObject(const char *vertexFilename, const char *fragmentFilename, aie::OBJMesh mesh)
{
	// load vertex shader from file
	m_shader.loadShader(aie::eShaderStage::VERTEX, vertexFilename);

	// load fragment shader from file
	m_shader.loadShader(aie::eShaderStage::FRAGMENT, fragmentFilename);
	if (m_shader.link() == false)
	{
		printf("Shader Error: %s\n", m_shader.getLastError());
	}

	// set default transform
	m_transform = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };

	m_light.direction = { 0.0f, 0.0f, 0.0f };
	m_light.diffuse = { 1, 1, 1 };
	m_light.specular = { 1.0f, 1.0f, 1.0f };
	m_ambientLight = { 0.25f, 0.25f, 0.25f };


	m_vertexChanged = false;
	m_fragmentChanged = false;
}


GameObject::~GameObject()
{
}

void GameObject::update(float deltaTime)
{
	if (m_vertexChanged)
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, m_vertFilename);
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}
	}
	if (m_fragmentChanged)
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, m_fragFilename);
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}
	}
}

void GameObject::draw()
{
	// bind shader
	m_shader.bind();

	// bind light
	m_shader.bindUniform("Ia", m_ambientLight);
	m_shader.bindUniform("Id", m_light.diffuse);
	m_shader.bindUniform("Is", m_light.specular);
	m_shader.bindUniform("lightDirection", m_light.direction);
	m_shader.bindUniform("roughness", m_roughness);
	m_shader.bindUniform("reflectionCoefficient", m_reflectionCoefficient);

	// bind transform for lighting
	m_shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_transform)));

	// bind transform
	auto pvm = m_projectionMatrix * m_viewMatrix * m_transform;
	m_shader.bindUniform("ProjectionViewModel", pvm);

	// bind camera
	//m_shader.bindUniform("cameraPosition", vec3 (glm::inverse(m_viewMatrix)[3]));
	m_shader.bindUniform("cameraPosition", m_camera.getPosition());

	// draw mesh
	m_mesh.draw();

}

glm::vec3 GameObject::getPosition()
{
	return m_position;
}

void GameObject::setVert(const char *filenae)
{
	m_vertexChanged = true;

}

void GameObject::setFrag(const char *filenae)
{
	m_fragmentChanged = true;
}

void GameObject::setMesh()
{
}

void GameObject::setPosition(glm::vec3 position)
{
}

void GameObject::setTransform(glm::mat4 transform)
{
}
