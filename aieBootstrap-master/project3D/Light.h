#pragma once
#include <glm/ext.hpp>
#include <list>
#include <glm\ext.hpp>

using glm::vec3;


struct Light {
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
