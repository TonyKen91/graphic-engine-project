#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui.h>

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

Application3D::Application3D() {

}

Application3D::~Application3D() {

}

bool Application3D::startup() {

	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
		getWindowWidth() / (float)getWindowHeight(),
		0.1f, 1000.f);

	// load vertex shader from file
	m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");

	// load fragment shader from file
	m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	if (m_shader.link() == false)
	{
		printf("Shader Error: %s\n", m_shader.getLastError());
	}

		m_light.direction = { 0.0f, 0.0f, 0.0f };
		m_light.diffuse = { 1, 1, 1 };
		m_light.specular = { 1.0f, 1.0f, 1.0f };
		m_light.ambient = { 0.25f, 0.25f, 0.25f };

	if (m_spearMesh.load("./soulspear/soulspear.obj", true, true) == false)
	{
		printf("Soulspear Mesh Error!\n");
		return false;
	}

	// Don't need to scale the model
	m_spearTransform = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };

	if (m_renderTarget.initialise(1, getWindowWidth(), getWindowHeight()) == false)
	{
		printf("Render Target Error! \n");
		return false;
	}

	// create a fullscree quad
	m_fullscreenQuad.initialiseFullscreenQuad();

	// load a post-processing shader
	m_postShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/post.vert");
	m_postShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/post.frag");

	if (m_postShader.link() == false)
	{
		printf("Post Shader Error: %s\n", m_postShader.getLastError());
	}
	return true;

}

void Application3D::shutdown() {

	Gizmos::destroy();
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	// rotate camera
	m_camera.Update();
	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
						vec3(-10 + i, 0, -10),
						i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
						vec3(-10, 0, -10 + i),
						i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	Gizmos::addTransform(mat4(1));

	// query time since3 application started
	time = getTime();

	// rotate light
	if (m_rotateLight)
		m_light.direction = glm::normalize(vec3(glm::cos(time * 2), glm::cos(time * 2), glm::sin(time * 2)));

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();


	if (input->isKeyDown(aie::INPUT_KEY_1))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}
	if (input->isKeyDown(aie::INPUT_KEY_2))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/texturedPhong.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/texturedPhong.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}
	if (input->isKeyDown(aie::INPUT_KEY_3))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/normalMap.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/normalMap.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}
	if (input->isKeyDown(aie::INPUT_KEY_4))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/OrenNayar.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/OrenNayar.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}
	if (input->isKeyDown(aie::INPUT_KEY_5))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/CookTorrance.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/CookTorrance.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}
	if (input->isKeyDown(aie::INPUT_KEY_6))
	{
		// load vertex shader from file
		m_shader.loadShader(aie::eShaderStage::VERTEX, "./shaders/ONCT.vert");

		// load fragment shader from file
		m_shader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/ONCT.frag");
		if (m_shader.link() == false)
		{
			printf("Shader Error: %s\n", m_shader.getLastError());
		}

	}


	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application3D::draw() {

	ImGui::Begin("Shader");
	ImGui::SliderFloat("Roughness", &m_roughness, 0.0f, 1.0f);
	ImGui::SliderFloat("ReflectionCoefficient", &m_reflectionCoefficient, 0.0f, 100.0f);
	ImGui::SliderInt("Blur Strength", &m_blurStrength, 0, 5);
	ImGui::SliderFloat("Distortion Strength", &m_distortionStrength, 0.0f, 100.0f);
	ImGui::End();


	char textboxName[40];
		//sprintf_s(textboxName, 40, "Directional Light %i", i + 1);
		//ImGui::Begin(textboxName);
	ImGui::Begin("Directional Light");
	ImGui::SliderFloat3("Light Direction", &m_light.direction.x, -1.0f, 1.0f);
	ImGui::SliderFloat3("Light Diffuse Colour", &m_light.diffuse.x, 0.0f, 1.0f);
	ImGui::SliderFloat3("Light Specular Colour", &m_light.specular.x, 0.0f, 1.0f);
	ImGui::SliderFloat3("Ambient Light Colour", &m_light.ambient.x, 0.0f, 1.0f);
	ImGui::Checkbox("Rotate Light", &m_rotateLight);
	ImGui::End();

	m_projectionMatrix = m_camera.GetProjectMatrix(getWindowWidth(), getWindowHeight());
	m_viewMatrix = m_camera.GetViewMatrix();

	// bind out render target
	m_renderTarget.bind();

	// wipe the screen to the background colour
	clearScreen();

	// bind shader
	m_shader.bind();

	// bind light
	m_shader.bindUniform("Ia", m_light.ambient);
	m_shader.bindUniform("Id", m_light.diffuse);
	m_shader.bindUniform("Is", m_light.specular);
	m_shader.bindUniform("lightDirection", m_light.direction);
	m_shader.bindUniform("roughness", m_roughness);
	m_shader.bindUniform("reflectionCoefficient", m_reflectionCoefficient);

	// bind camera
	//m_shader.bindUniform("cameraPosition", vec3 (glm::inverse(m_viewMatrix)[3]));
	m_shader.bindUniform("cameraPosition", m_camera.getPosition());


	auto pvm = m_projectionMatrix * m_viewMatrix * m_spearTransform;
	m_shader.bindUniform("ProjectionViewModel", pvm);

	// bind transform for lighting
	m_shader.bindUniform("ModelMatrix", m_spearTransform);
	m_shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_spearTransform)));

	// draw mesh
	m_spearMesh.draw();
	//m_bunnyMesh.draw();

	// draw 3D gizmos
	Gizmos::draw(m_projectionMatrix * m_viewMatrix);

	// draw 2D gizmos using an orthogonal projection matrix (or screen dimensions)
	Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());

	// unbind target to return to backbuffer
	m_renderTarget.unbind();
	
	// clear the backbuffer
	clearScreen();

	// bind texturing shader
	m_postShader.bind();
	m_postShader.bindUniform("colourTarget", 0);
	m_postShader.bindUniform("blurStrength", m_blurStrength);
	m_postShader.bindUniform("distortionStrength", m_distortionStrength);
	m_renderTarget.getTarget(0).bind(0);

	// draw quad
	m_fullscreenQuad.draw();

}