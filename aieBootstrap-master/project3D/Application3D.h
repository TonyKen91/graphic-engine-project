#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "tiny_obj_loader.h"
#include "RenderTarget.h"
#include "Light.h"

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;

	aie::Texture		m_gridTexture;

	aie::ShaderProgram	m_shader;

	aie::OBJMesh		m_spearMesh;
	glm::mat4			m_spearTransform;

	aie::ShaderProgram	m_postShader;
	Mesh				m_fullscreenQuad;


	Camera				m_camera;

	aie::Texture		m_testTexture;

	aie::RenderTarget	m_renderTarget;
	
	float				m_roughness = 0.0f;
	float				m_reflectionCoefficient = 0.0f;
	int					m_blurStrength = 0;
	float				m_distortionStrength = 0.0f;
	bool				m_rotateLight = false;
	float				m_numberOfLights = 3;

	Light			m_light;

};